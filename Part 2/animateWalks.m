function [ f ] = animateWalks( map, simulated_walks, frames_to_skip, delay )
%ANIMATEWALKS Summary of this function goes here
%   Detailed explanation goes here
X_INDEX = 1; Y_INDEX = 2;

f = figure(gcf);
[m, ~] = size(simulated_walks);

pause_flag = false;
if (nargin == 4 && ~isnan(delay))
    pause_flag = true;
end

try
    for i = 1:frames_to_skip:m
        figure(f);
        imshow(map, 'InitialMagnification', 'fit');
        hold on;
        %%%% Show last two steps %%%%
        if(i > 2)
            plot(simulated_walks(i-2, :, X_INDEX), simulated_walks(i-2, :, Y_INDEX), 'og');
            plot(simulated_walks(i-1, :, X_INDEX), simulated_walks(i-1, :, Y_INDEX), 'ob');
        end

        plot(simulated_walks(i, :, X_INDEX), simulated_walks(i, :, Y_INDEX), '*r');
        hold off;
        title(['M = ', num2str(i-1)]);

        if pause_flag
            pause(delay)
        end
        drawnow;
    end

catch error
    if (strcmp(error.identifier, ...
            'MATLAB:hgbuiltins:object_creation:InvalidConvenienceArgHandle'))
        disp('animation closed');
    else
        throw(error)
    end
end

%%% END OF FUNCTION FILE
end

