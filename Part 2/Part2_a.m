clear all, clc, close all;
HEIGHT = 200; 
WIDTH = 200;
OBSTACLE_DENSITY = 0.1;
USING_FENCE = false;
NUM_DIRECTIONS = 4; % north, south, east, west

M = 750; % Number of steps a walker makes
N = 1000; % Number of walkers

walker_position = [floor(WIDTH/2), floor(HEIGHT/2)];

map = true(HEIGHT, WIDTH);

%% Generate obstacles and place on map
obstacles_mask = rand(HEIGHT, WIDTH) < OBSTACLE_DENSITY;
 
fence_x = [80, 90, 110, 120];
fence_y = [80, 90, 110, 120];

% fence_x = [8, 12]; %use for smaller frame
% fence_y = [8, 12]; %use for smaller frame

if (USING_FENCE)
    map(fence_x, :) = ~obstacles_mask(fence_x, :);
    map(:, fence_y) = ~obstacles_mask(:, fence_y);
else
    map = ~obstacles_mask;
end

% Make the central position available for the walker
map(walker_position(1), walker_position(2)) = true;

%% Calculate Walks
direction_angles = generateActions(M, N, NUM_DIRECTIONS);

simulated_walks = zeros(M+1, N, 2);

% Defining the original position for walkers
x0 = walker_position(1); y0 = walker_position(2);
X_INDEX = 1; Y_INDEX = 2;

simulated_walks(1, :, X_INDEX) = x0;
simulated_walks(1, :, Y_INDEX) = y0;

% Calculating the moves for the walkers' steps
moves_map = simulated_walks;
moves_map(2:end, :, X_INDEX) = round(cos(direction_angles));
moves_map(2:end, :, Y_INDEX) = round(sin(direction_angles));

for i = 2:M+1
    x_step = moves_map(i, :, X_INDEX);
    y_step = moves_map(i, :, Y_INDEX);
    
    % calculate next position of walkers 
    new_x = simulated_walks(i-1, :, X_INDEX) + x_step;
    new_y = simulated_walks(i-1, :, Y_INDEX) + y_step;
    
    % determine if walker has gone out of the boundaries
    x_invalid_mask = (new_x <= 0) | (new_x > WIDTH) | isnan(new_x);
    y_invalid_mask = (new_y <= 0) | (new_y > HEIGHT) | isnan(new_y);
    valid_mask = ~x_invalid_mask & ~y_invalid_mask;
     
    % check if walkers collided with fence
    field_mask = false(size(new_x));
    for j = 1:length(field_mask)
        if (~isnan(new_x(j)) && ~isnan(new_y(j)) && valid_mask(j))
            field_mask(j) = map(new_y(j), new_x(j));
        end
    end
    
    % walkers move to points that don't collide into fence/obstacle
    simulated_walks(i, field_mask, X_INDEX) = new_x(field_mask);
    simulated_walks(i, field_mask, Y_INDEX) = new_y(field_mask);
    
    % walkers stay in the same position for collision of obstacles/fences
    simulated_walks(i, ~field_mask, X_INDEX) = simulated_walks(i-1, ~field_mask, X_INDEX);
    simulated_walks(i, ~field_mask, Y_INDEX) = simulated_walks(i-1, ~field_mask, Y_INDEX);
    
    % points that have gone outside of the boundaries are forever doomed
    simulated_walks(i, ~valid_mask, :) = NaN;
end

%% Calculate MSD
displacement_map = sqrt((simulated_walks(:, :, X_INDEX) - x0).^2 ...
    + (simulated_walks(:, :, Y_INDEX) - y0).^2);

squared_displacement = displacement_map.^2;
MSD = 1/N*sum(squared_displacement,2);

%% Display steps for walkers on the map
frames_to_skip = 10;
delay = 0.0;
animateWalks(map, simulated_walks, frames_to_skip, delay);

