function [ direction_angles ] = generateActions( num_steps, num_walkers, num_directions )
%GENERATEACTIONS Generates the angular direction for every step and path
%based on the number of available directions. Assumes an equal probability 
%for all directions.
    if nargin < 3
        num_directions = 4;
    end
    
    CIRCLE_ANGLE = 2 * pi;
    
    cdf = 0:1/num_directions:1;
    
    random_walks = rand(num_steps, num_walkers);
    
    direction_angles = zeros(size(random_walks));
    for i = 1:num_directions
         direction_mask = cdf(i) <= random_walks & random_walks < cdf(i+1);
         direction_angles(direction_mask) = cdf(i);
    end
    
    direction_angles = direction_angles * CIRCLE_ANGLE;
end

