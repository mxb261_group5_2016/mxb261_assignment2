clear all, close all, clc;
N = 500; % Number of walkers
M = 500; % Number of steps

simulated_walks = zeros(M+1, N);

% Defining the original position for walkers
x0 = 0; 
simulated_walks(1, :) = x0;

% Generating the random values and defining the directions
NUM_DIRECTIONS = 2; % 2 directions -> left and right
direction_angle = generateActions(M, N, NUM_DIRECTIONS); % should give 0 or pi 

% left value is -1, right value is 1
move_map = round(cos(direction_angle));

% Creating a map of moves that the walkers make though each step
moves = simulated_walks;
moves(2:end, :) = moves(2:end, :) + move_map;

% Calculating the final position of the walkers after each step
simulated_walks = cumsum(moves);

squared_displacement = (simulated_walks - x0).^2;
MSD = sum(squared_displacement')/N;

%% Histogram Animation
% Video settings

delay_per_frame = 0.25;
fps = 1/delay_per_frame; %frames per second

% Initialize video object
oRandomWalkVideo = VideoWriter(['random_walk_1a_', num2str(fps), 'fps']); 
oRandomWalkVideo.FrameRate = fps; 
open(oRandomWalkVideo);


FRAME_SKIP = 10;

h = figure;
for i = 1:FRAME_SKIP:M+1
    figure(h);
    
    histogram(simulated_walks(i, :)); 
    hold on;
    xspace = max(abs(simulated_walks(i,:)));
    axis([-xspace-1 xspace+1 0 100]) %%-1 and +1 to space out the values on the first iteration
    title(['M = ', num2str(i-1)]);
    writeVideo(oRandomWalkVideo, getframe(h))
    hold off;
end
writeVideo(oRandomWalkVideo, getframe(h)) % write the frame again
close(oRandomWalkVideo);
