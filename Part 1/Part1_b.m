clear all, close all, clc;
%% Initialisations
N = 5; % Number of walkers
M = 5; % Number of steps

simulated_walks = zeros(M+1, N, 2);
displacement_map = simulated_walks; % copy blank of simulated walks

% Defining the original position for walkers
x0 = 0; y0 = 0;
X_INDEX = 1; Y_INDEX = 2;

simulated_walks(1, :, X_INDEX) = x0;
simulated_walks(1, :, Y_INDEX) = y0;

% Number of possible directions
NUM_DIRECTIONS = 4;

%%
% Creating a map of moves that the walkers make though each step
direction_angles = generateActions(M, N, NUM_DIRECTIONS);
moves_map = simulated_walks;
moves_map(2:end, :, X_INDEX) = round(cos(direction_angles));
moves_map(2:end, :, Y_INDEX) = round(sin(direction_angles));

% Calculating the final position of the walkers after each step
simulated_walks = cumsum(moves_map);

displacement_map = sqrt((simulated_walks(:, :, X_INDEX) - x0).^2 ...
    + (simulated_walks(:, :, Y_INDEX) - y0).^2);
MSD = sum(displacement_map.^2,2)/N;
FRAME_SKIP = 10;

f = figure;
hold on;
for i = 1:N
    figure(f);
    plot(simulated_walks(:, i, X_INDEX), simulated_walks(:, i, Y_INDEX))
end
axis equal;
hold off;

%% Animate scatter plot

% f2 = figure;
% delay = 0.2
% for i = 1:FRAME_SKIP:M+1
%     figure(f2);
%     scatter(simulated_walks(i, :, X_INDEX), simulated_walks(i, :, Y_INDEX), '*');
%     title(['M = ', num2str(i-1)]);
%     axis equal;
%     pause(delay);
% end
% hold off;
