function [ age_map ] = updateParasiteAge( moved, origin, age_map )
%UPDATEPARASITEAGE Summary of this function goes here
%   Update parasite age and relative positions
    age_map = age_map + 1;
    age_map(moved{Compass.north}) = age_map(origin{Compass.north});
    age_map(moved{Compass.south}) = age_map(origin{Compass.south});
    age_map(moved{Compass.east}) = age_map(origin{Compass.east});
    age_map(moved{Compass.west}) = age_map(origin{Compass.west});
    
    age_map(origin{Compass.north} ...
        | origin{Compass.south} ...
        | origin{Compass.east} ...
        | origin{Compass.west}) = NaN;    
end
