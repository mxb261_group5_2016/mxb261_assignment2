function [ origin ] = originMovedParasites( moved_parasite_positions )
%ORIGINMOVEDPARASITES Summary of this function goes here
%   Detailed explanation goes here
    shifted_pn = WrapAround.shiftDown(moved_parasite_positions{Compass.north});
    shifted_ps = WrapAround.shiftUp(moved_parasite_positions{Compass.south});
    shifted_pe = WrapAround.shiftLeft(moved_parasite_positions{Compass.east});
    shifted_pw = WrapAround.shiftRight(moved_parasite_positions{Compass.west});
    
    origin = {shifted_pn, shifted_ps, shifted_pe, shifted_pw};
end
