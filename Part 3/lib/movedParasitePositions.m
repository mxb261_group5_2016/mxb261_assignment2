function [ moved_parasite_positions ] = movedParasitePositions( action_map, parasites_map, nsew )
%NEXTPARASITEPOSITIONS Summary of this function goes here
%   Detailed explanation goes here
    north = (action_map <= nsew(1)) & parasites_map;
    
    south = ((action_map > nsew(1)) ...
        & (action_map <= nsew(2))) & parasites_map;
    
    east = ((action_map > nsew(2)) ...
        & (action_map <= nsew(3))) & parasites_map;
    
    west = ((action_map > nsew(3)) ...
        & (action_map <= nsew(4))) & parasites_map;

    % Wrap around condition for attempted parasite movement
    p_n = WrapAround.shiftUp(north);
    p_s = WrapAround.shiftDown(south);
    p_e = WrapAround.shiftRight(east);
    p_w = WrapAround.shiftLeft(west);
    
    % Get next parasite positions that don't collide into each other
    next_pn = p_n & ~(p_e | p_w | p_s) & ~parasites_map;
    next_ps = p_s & ~(p_n | p_w | p_e) & ~parasites_map;
    next_pe = p_e & ~(p_n | p_w | p_s) & ~parasites_map;
    next_pw = p_w & ~(p_n | p_e | p_s) & ~parasites_map;
    
    moved_parasite_positions = {next_pn, next_ps, next_pe, next_pw};
end

