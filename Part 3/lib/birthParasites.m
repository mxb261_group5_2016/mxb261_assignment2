function [ baby_parasites ] = birthParasites( consumed_map )
%BIRTHPARASITES Summary of this function goes here
%   Detailed explanation goes here
    baby_parasites = WrapAround.shiftDown(consumed_map{Compass.north}) ... % north parent
        | WrapAround.shiftUp(consumed_map{Compass.south}) ... % south parent
        | WrapAround.shiftLeft(consumed_map{Compass.east}) ... % east parent
        | WrapAround.shiftRight(consumed_map{Compass.west}); % west parent
end

