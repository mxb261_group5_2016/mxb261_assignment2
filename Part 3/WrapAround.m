classdef WrapAround
    %WRAPAROUND Summary of this class goes here
    %   Detailed explanation goes here    
    methods(Static)
        function [ output_map ] = shiftUp( input_map )
           output_map = [input_map(2:end, :); input_map(1, :)];
        end

        function [ output_map ] = shiftDown( input_map )
           output_map = [input_map(end, :); input_map(1:end-1, :)];
        end
        
        function [ output_map ] = shiftLeft( input_map )
           output_map = [input_map(:, 2:end), input_map(:, 1)];
        end
        
        function [ output_map ] = shiftRight( input_map )
           output_map = [input_map(:, end), input_map(:, 1:end-1)];
        end
    end    
end

